package Dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = loginManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "select*from user where login_id =? and password = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			String result = passpass(password);
			stmt.setString(2, result);
			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = loginManager.getConnection();

			String sql = "SELECT * FROM user where login_id not in ('admin')";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return userList;
	}

	public List<User> findByWord(String loginId, String name, String fromDate, String toDate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = loginManager.getConnection();

			String sql = "SELECT * FROM user where login_id not in ('admin')";

			if(!(loginId.equals(""))) {
				sql += " AND login_id = '" + loginId +"'";
			}

			if(!(name.equals(""))) {
				sql += " AND name like '%" + name +"%'";
			}

			if(!(fromDate.equals(""))) {
				sql += " AND birth_date >= '" + fromDate +"'";
			}

			if(!(toDate.equals(""))) {
				sql += " AND birth_date <= '" + toDate +"'";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId1 = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId1, name1, birthDate, password, createDate, updateDate);

				userList.add(user);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return userList;
	}

	public User InsertIntoCreateInfo(String loginId, String password, String passwordConf, String userName,
			String birthDate) {
		Connection conn = null;
		try {
			conn = loginManager.getConnection();

			String sql = "INSERT INTO user(login_id,password,name,birth_date,create_date,update_date) VALUES(?,?,?,?,now(),now())";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			String result = passpass(password);
			stmt.setString(2, result);
			stmt.setString(3, userName);
			stmt.setString(4, birthDate);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public User findBykakuninInfo(String loginId) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			// データベースへ接続
			conn = loginManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "select*from user where login_id=?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);

			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");

			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User findByDeleteInfo(String loginId) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			conn = loginManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "delete from user where login_id=?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public User findBySentakuInfo(int id) {
		Connection conn = null;
		User user = null;

		try {
			conn = loginManager.getConnection();

			String sql = "SELECT * FROM user where id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id2 = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			user = new User(id2, loginId, name, birthDate, password, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return user;
	}

	public User findByDetailInfo(int id) {
		Connection conn = null;
		User user = null;

		try {
			conn = loginManager.getConnection();

			String sql = "SELECT * FROM user where id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id2 = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			user = new User(id2, loginId, name, birthDate, password, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return user;
	}

	public User findByUpdateInfo(int id, String password, String userName, String birthDate, String updateDate) {
		Connection conn = null;
		User user = null;

		try {
			conn = loginManager.getConnection();

			String sql = "UPDATE user set name=?,birth_date=?,password=?,update_date=now() where id=?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, userName);
			stmt.setString(2, birthDate);
			String result = passpass(password);
			stmt.setString(3, result);
			stmt.setInt(4, id);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return user;
	}

	public User findByUpdate2Info(int id, String userName, String birthDate, String updateDate) {

		Connection conn = null;
		User user = null;

		try {
			conn = loginManager.getConnection();

			String sql = "UPDATE user set name=?,birth_date=?,update_date=now() where id=?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, userName);
			stmt.setString(2, birthDate);
			stmt.setInt(3, id);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return user;
	}

	public String passpass(String password) {
		String result = null;

			//暗号化MD5
			String source = password;

			Charset charset = StandardCharsets.UTF_8;

			String algorithm = "MD5";
			byte[] bytes = null;
			try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				return null;
			}
			result = DatatypeConverter.printHexBinary(bytes);

		return result;
	}
}
