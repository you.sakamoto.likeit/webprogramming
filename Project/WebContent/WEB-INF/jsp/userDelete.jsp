<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除確認</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">

</head>
<body>

<form class="form-delete" action=UserDeleteServlet method="post">
<div class="row">
        <div class="col-sm-11"></div>
	<a class="btn" href="LogOutServlet">ログアウト</a>
		</div>

		  <!-- body -->
  <div class="container">

	<p style="margin:50px;"></p>
	<div class="row">
        <div class="col-sm-4"></div>
	<h1>ユーザー削除確認</h1>
		</div>
		<p style="margin:50px;"></p>

 <div class="container">
 <div class="delete-area">
 <div class="row">
        <div class="col-sm-5"></div>
      <p>ログインID:<a>${user.loginId}</a>を削除しますか？</p>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <a href="UserListServlet" class="btn btn-light btn-block">いいえ</a>
        </div>
        <div class="col-sm-6">
          <button type="submit" value="${user.loginId}" class="btn btn-light btn-block"name="login_id">はい</button>
        </div>
      </div>
    </div>
  </div>
  <div><a class="btn" href="UserListServlet">戻る</a></div>
</div>
</form>
</body>
</html>