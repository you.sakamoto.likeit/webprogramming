<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
</head>
<body>


<form class="form-signin" action="LoginServlet" method="post">

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<p style="margin:100px;"></p>
	<div class="row">
        <div class="col-sm-5"></div>
	<h1>ログイン画面</h1>
		</div>

	<p style="margin:100px;"></p>
	<div class="row">
        <div class="col-sm-4"></div>
	<h3>ログインID</h3>
	<h1>　　</h1>
 	<input type="text" style="width:300px;" class="form-control" name="loginId">
		</div>
	</div>

	<p style="margin:50px;"></p>
	<div class="row">
        <div class="col-sm-4"></div>
	<h3>パスワード</h3>
	<h1>　　</h1>
 	<input type="text" style="width:300px;" class="form-control" name="password">
		</div>
	</div>

	<p style="margin:50px;"></p>
	<div class="row">
        <div class="col-sm-5"></div>
        <h1>　</h1>
	 <button type="submit">　　　ログイン　　　</button>
    </form>
		</div>
	</div>

	</body>
	</html>