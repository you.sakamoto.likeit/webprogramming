<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
</head>
<body>

<form class="form-create" action=UserCreate method="post">

	<div class="row">
        <div class="col-sm-11"></div>
	<a class="btn" href="LogOutServlet">ログアウト</a>
		</div>


		  <!-- body -->
  <div class="container">

  <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<p style="margin:50px;"></p>
	<div class="row">
        <div class="col-sm-4"></div>
	<h1>ユーザー新規登録</h1>
		</div>
		<p style="margin:50px;"></p>

  <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
      <input type="text" id="text3a" class="form-control"name="loginId">
    </div>
  </div>
  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-10">
      <input type="password" id="password" class="form-control" name="password">
    </div>
  </div>

 <div class="form-group row">
    <label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
    <div class="col-sm-10">
      <input name="passwordConf" type="password" id="passwordConf" class="form-control">
    </div>
  </div>

<div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
          <input type="text" id="userName" class="form-control" name="userName">
        </div>
      </div>

      <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-10">
          <input type="date" id="birthDate" class="form-control" name="birthDate">
        </div>
      </div>

<div><button type="submit" value="#" class="btn btn-primary btn-block">登録</button><br></div>

<div><a class="btn" href="UserListServlet">戻る</a></div>

</form>

</body>
</html>