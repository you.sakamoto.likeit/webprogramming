<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">

</head>


<form class="form-userlist" action="UserListServlet" method="post">
	<div class="row">
        <div class="col-sm-11"></div>
	<a class="btn" href="LogOutServlet">ログアウト</a>
		</div>

	  	<p style="margin:50px;"></p>
	<div class="row">
        <div class="col-sm-9"></div>
         <c:if test="${userInfo.loginId=='admin'}">
		    <a class="btn btn-primary" href="UserCreate">新規登録</a>
		    </c:if>
	</div>


<body>

	<!-- body -->
  <div class="container">

	<p style="margin:100px;"></p>
	<div class="row">
        <div class="col-sm-5"></div>s
	<h1>ユーザー一覧</h1>
		</div>

<!-- 新規登録ボタン -->


	 <div class="form-group row">
    <label for="loginId" class="col-sm-2 col-form-label" >ログインID</label>
    <div class="col-sm-10">
      <input type="text" id="loginId" class="form-control"name="loginId">
    </div>
  </div>
 <div class="form-group row">
   <label for="userName" class="col-sm-2 col-form-label">ユーザー名</label>
     <div class="col-sm-10">
      <input type="text" id="loginId" class="form-control"name="userName">
     </div>
  </div>

  <div class="row">
  <div class="form-group col-sm-5">
    <label for="birthDate">生年月日</label>
    <input type="date" class="date-start" id="date-start" placeholder="年/月/日" name="saki">
  </div>
  <div>～</div>
  <div class="form-group col-sm-5">
    <label for="birthDate">生年月日</label>
    <input type="date" class="date-end" id="date-end" placeholder="年/月/日" name="ato">
  </div>
</div>

<input type="hidden" class="form-control-plaintext"${user.birthDate} name="birth_date">

<div class = text-right>
	<button type="submit" value="#" class="btn btn-outline-danger">検索</button>

<hr size="5" color="red" noshade>



        <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>



                     <c:if test="${userInfo.loginId=='admin'}">

                            <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>

 					</c:if>

                     <c:if test="${user.loginId==userInfo.loginId}">
   						 <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                     </c:if>
 					<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>
         </div>
      </div>
    </div>

</body>
</form>
</body>
</html>