<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報詳細参照</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
</head>
<body>

 	<div class="row">
        <div class="col-sm-11"></div>
	<a class="btn" href="LogOutServlet">ログアウト</a>
		</div>

  <div class="container">

	<p style="margin:50px;"></p>
	<div class="row">
        <div class="col-sm-4"></div>
	<h1>ユーザー情報詳細参照</h1>
		</div>
		<p style="margin:50px;"></p>


  <form method="post" action="#" class="form-horizontal">
      <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.loginId}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.name}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.birthDate}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="createDate" class="col-sm-2 col-form-label">新規登録日時</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.createDate}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.updateDate}</p>
        </div>
      </div>

      <div class="col-xs-4">
        <a href="UserListServlet">戻る</a>
      </div>

    </form>


  </div>
  </div>

</body>
</html>