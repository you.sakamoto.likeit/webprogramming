<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
<link rel="stylesheet"
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous">
</head>
<body>

<form class="form-update" action=UserUpdateServlet method="post">
	<input type="hidden" name="id" value="${user.id}" >

	<div class="row">
        <div class="col-sm-11"></div>
	<a class="btn" href="LogOutServlet">ログアウト</a>
		</div>

		<div class="container">

		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


	<p style="margin:50px;"></p>
	<div class="row">
        <div class="col-sm-4"></div>
	<h1>ユーザー情報更新</h1>
		</div>
		<p style="margin:50px;"></p>

		ログインID　　　　　　　${user.loginId}


 <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-10">
      <input type="password" id="password" class="form-control"name="password" >
    </div>
  </div>

 <div class="form-group row">
    <label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
    <div class="col-sm-10">
      <input type="password" id="passwordConf" class="form-control"name="passwordConf">
    </div>
  </div>

<div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
          <input type="text" id="userName" class="form-control"  value="${user.name}" name=userName>
        </div>
      </div>

      <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-10">
          <input type="date" id="birthDate" class="form-control" value="${user.birthDate}" name=birthDate>
        </div>
      </div>


<input type="hidden" id="update_date"  name="update_date">


<button type="submit" value="${user.loginId}" class="btn btn-primary">更新</button><br>

<div><a class="btn" href="UserListServlet">戻る</a></div>

</body>
</html>